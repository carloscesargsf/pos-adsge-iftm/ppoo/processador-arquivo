# Processador

## Princípios SOLID

### Single Responsibility Principle
    Uso nos arquivos presentes em "core/tipoRegistro", nos quais são realizados os parses das linhas do arquivo de
    acordo com o formato do mesmo.

### Open-Closed Principle
    Definição de interfaces para processamento das linhas de um arquivo (core/processador/ProcessadorArquivo), sendo as
    implementações realizadas em "core/processador/ProcessadorArquivo/arquivo[XXX]".

### Liskov Substitution Principle
    Uso nas interfaces "DefaultService", "CreateService" (a qual extende "DefaultService"), além de
    "CreateUpdateService" (a qual extende "CreateService"), todas localizadas em "core/service". Isso permitiu a
    generalização do tipo de atributo utilizado em "core/processador/arquivo[XXX]/TipoRegistro". Utilizado em

### Interface Segregation Principle
    Uso nas interfaces "DefaultService", "CreateService" (a qual extende "DefaultService"), além de
    "CreateUpdateService" (a qual extende "CreateService"), todas localizadas em "core/service".

### Dependency Inversion Principle
    Uso de injeção de dependências do Spring Boot, com auxílio do "core/BeanProvider" para que seja possível executar o
    projeto através da linha de comando.

## Estrutura

### ProcessadorDeArquivosApplication

	Função main do projeto. Lê o arquivo a ser importado e coordena a sua importação, de acordo com a ordem definida em "enums/ArquivoProcessamento".

### core/dto/DefaultDTO

	Interface para padronizar o objeto de transferência de dados do tipo de registro a ser persistido.

### core/dto/[XXX]DTO

	Implementação da interface acima para cada tipo de registro a ser persistido.

### core/entity/[XXX]

	Implementação do tipo de registro a ser persistido.

### core/service/DefaultService

	Classe abstrata para definir o método padrão para persistir o objeto criado, além de realizar a injeção de dependências.

### core/service/[XXX]Service

	Extensão da classe abstrata para persistir cada tipo de registro.

### core/tipoRegistro/ProcessadorTipoRegistro

	Interface para padronizar o método de parseamento da linha do arquivo.

### core/tipoRegistro/ProcessadorTipoRegistro[XXX]

	Implementação da interface citada acima. Cria o objeto a ser persistido no banco de dados, através da linha do arquivo sendo processado.

### core/processador/ProcessadorArquivo

	Interface para padronizar o método para processamento do arquivo.

### core/processador/arquivo[XXX]/ProcessadorArquivo[XXX]

	Implementação da interface citada acima. Processa as linhas do arquivo do tipo [XXX].

	Verifica qual o tipo de registro, através do arquivo "TipoRegistro" (enum para decisão do tipo de registro a ser processado) presente no mesmo diretório.

	Executa, por reflection, o método "salvarRegistro", passando o DTO a ser persistido.

