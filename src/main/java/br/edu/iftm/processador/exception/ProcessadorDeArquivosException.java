package br.edu.iftm.processador.exception;

public class ProcessadorDeArquivosException extends RuntimeException {

    public ProcessadorDeArquivosException(String message) {
        super(message);
    }

    public ProcessadorDeArquivosException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ProcessadorDeArquivosException(Throwable throwable) {
        super(throwable);
    }

}
