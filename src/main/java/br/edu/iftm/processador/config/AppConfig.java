package br.edu.iftm.processador.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.edu.iftm.processador")
public class AppConfig {
}
