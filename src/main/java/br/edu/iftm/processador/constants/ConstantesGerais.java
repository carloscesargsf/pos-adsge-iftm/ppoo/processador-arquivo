package br.edu.iftm.processador.constants;

public class ConstantesGerais {

    public static final String REGISTRO_JA_EXISTE = "Registro já existe!";
    public static final String REGISTRO_NAO_ENCONTRADO = "Registro não encontrado!";
    public static final String NENHUM_REGISTRO_ENCONTRADO = "Nenhum registro encontrado!";
    public static final String ERRO_AO_MAPEAR_ENTIDADE = "Erro ao mapear entidade.";
    public static final String LOTE_JA_PROCESSADO = "Lote %s do tipo arquivo %s já foi processado.";
    public static final String LOTE_SEM_PROCESSAR = "Os seguintes lotes estão sem processar: %s";

    private static final String CAMINHO_BASE = System.getProperty("user.dir");
    public static final String CAMINHO_BASE_ARQUIVOS_A_PROCESSAR = CAMINHO_BASE + "\\files\\a_processar\\";
    public static final String CAMINHO_BASE_ARQUIVOS_PROCESSADOS = CAMINHO_BASE + "\\files\\processados\\";

    public static final String ACAO_INCLUSAO = "I";
    public static final String ACAO_ALTERACAO = "A";
    public static final String ACAO_NAO_IMPLEMENTADA = "Ação não implementada.";

    public static final String PT_BR_DATE_FORMATTER = "dd/MM/yyyy";
    public static final String PT_BR_DATETIME_FORMATTER_ONLY_NUMBERS = "ddMMyyyyHHmmss";
    public static final String EN_US_DATE_FORMATTER = "yyyy-MM-dd";
    public static final String EN_US_DATE_FORMATTER_ONLY_NUMBERS = "yyyyMMdd";

    public static final String ONLY_UNDERSCORE_STRING_PATTERN = "^([_])\\1+$";

}
