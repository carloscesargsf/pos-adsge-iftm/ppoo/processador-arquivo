package br.edu.iftm.processador.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatterUtils {

    public static String formatarData(String dateTimeFormatterString) {
        return formatarData(dateTimeFormatterString, LocalDateTime.now());
    }

    public static String formatarData(String dateTimeFormatterString, LocalDateTime data) {
        return data.format(DateTimeFormatter.ofPattern(dateTimeFormatterString));
    }

    public static Integer converterParaInteger(String numero) {
        return Integer.parseInt(numero);
    }

    public static Double converterParaDouble(String numero, Integer precisao) {
        return Double.parseDouble(numero) / Math.pow(10d, precisao);
    }

    public static LocalDateTime converterParaLocalDateTime(String data, String pattern) {
        return LocalDateTime.parse(data, DateTimeFormatter.ofPattern(pattern));
    }

}
