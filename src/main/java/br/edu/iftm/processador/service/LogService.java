package br.edu.iftm.processador.service;

import java.time.LocalDateTime;

import static java.lang.System.out;

public class LogService {

    public static void imprimeLog(String log) {
        out.println(LocalDateTime.now() + " - " + log);
    }

}
