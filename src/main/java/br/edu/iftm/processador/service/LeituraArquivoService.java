package br.edu.iftm.processador.service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LeituraArquivoService {

    private static BufferedReader abrirArquivo(String arquivo) throws FileNotFoundException {
        File file = new File(arquivo);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        return br;
    }

    public static List<String> lerArquivo(String arquivo) throws IOException {
        BufferedReader br = abrirArquivo(arquivo);
        List<String> lista = new ArrayList<>();
        while (br.ready()) {
            lista.add(" " + br.readLine());
        }
        fechaArquivo(br);
        return lista;
    }

    private static void fechaArquivo(BufferedReader br) throws IOException {
        br.close();
    }

}
