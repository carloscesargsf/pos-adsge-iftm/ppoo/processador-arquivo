package br.edu.iftm.processador.repository;

import br.edu.iftm.processador.core.entity.Plastico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlasticoRepository extends JpaRepository<Plastico, Integer> {

    Optional<Plastico> findByNumeroPlastico(Integer numeroPlastico);

}
