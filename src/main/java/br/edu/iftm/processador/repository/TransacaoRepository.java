package br.edu.iftm.processador.repository;

import br.edu.iftm.processador.core.entity.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Integer> {

    Optional<Transacao> findByNumeroTransacao(Integer numeroTransacao);

}
