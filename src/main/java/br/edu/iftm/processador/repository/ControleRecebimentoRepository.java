package br.edu.iftm.processador.repository;

import br.edu.iftm.processador.core.entity.ControleRecebimento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ControleRecebimentoRepository extends JpaRepository<ControleRecebimento, Integer> {

    Optional<ControleRecebimento> findByNumeroLote(Integer numeroLote);

    Optional<ControleRecebimento> queryFirst1ByTipoArquivoOrderByNumeroLoteDesc(String tipoArquivo);

}
