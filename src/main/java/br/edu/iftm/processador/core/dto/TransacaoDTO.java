package br.edu.iftm.processador.core.dto;

import br.edu.iftm.processador.core.entity.Transacao;

import java.time.LocalDateTime;

public class TransacaoDTO implements DefaultDTO {

    private Integer numeroTransacao;

    private Integer numeroConta;

    private Integer numeroPlastico;

    private Double valor;

    private LocalDateTime dataTransacao;

    private Integer codigoLoja;

    public TransacaoDTO() {
    }

    public TransacaoDTO(Transacao transacao) {
        setNumeroTransacao(transacao.getNumeroTransacao());
        setNumeroConta(transacao.getNumeroConta());
        setNumeroPlastico(transacao.getNumeroPlastico());
        setValor(transacao.getValor());
        setDataTransacao(transacao.getDataTransacao());
        setCodigoLoja(transacao.getCodigoLoja());
    }

    public Integer getNumeroTransacao() {
        return numeroTransacao;
    }

    public void setNumeroTransacao(Integer numeroTransacao) {
        this.numeroTransacao = numeroTransacao;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Integer getNumeroPlastico() {
        return numeroPlastico;
    }

    public void setNumeroPlastico(Integer numeroPlastico) {
        this.numeroPlastico = numeroPlastico;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public LocalDateTime getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(LocalDateTime dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    public Integer getCodigoLoja() {
        return codigoLoja;
    }

    public void setCodigoLoja(Integer codigoLoja) {
        this.codigoLoja = codigoLoja;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        TransacaoDTO that = (TransacaoDTO) o;
//        return numeroTransacao.equals(that.numeroTransacao);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(numeroTransacao);
//    }
//
//    @Override
//    public String toString() {
//        return "Transacao{" +
//                "numeroTransacao=" + numeroTransacao +
//                ", numeroConta=" + numeroConta +
//                ", numeroPlastico=" + numeroPlastico +
//                ", valor=" + valor +
//                ", dataTransacao=" + dataTransacao +
//                ", codigoLoja=" + codigoLoja +
//                '}';
//    }

}
