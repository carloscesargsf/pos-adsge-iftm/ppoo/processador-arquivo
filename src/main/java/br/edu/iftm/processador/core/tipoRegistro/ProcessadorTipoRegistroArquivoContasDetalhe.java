package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.dto.ContaDTO;
import br.edu.iftm.processador.core.dto.DefaultDTO;

import java.util.regex.Pattern;

import static br.edu.iftm.processador.constants.ConstantesGerais.ONLY_UNDERSCORE_STRING_PATTERN;
import static br.edu.iftm.processador.utils.FormatterUtils.converterParaDouble;
import static br.edu.iftm.processador.utils.FormatterUtils.converterParaInteger;

public class ProcessadorTipoRegistroArquivoContasDetalhe extends ProcessadorTipoRegistro {

    public DefaultDTO processaLinha(String linha) {
        String acao = String.valueOf(linha.charAt(2));
        String cpf = linha.substring(3, 14).trim();
        String valorLimite = linha.substring(14, 26).trim();
        String diaVencimento = linha.substring(26, 28).trim();
        String numeroConta = linha.substring(28, 35).trim();

        ContaDTO contaDTO = new ContaDTO();

        contaDTO.setAcao(acao);
        contaDTO.setCpf(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, cpf)
                ? cpf
                : "");
        contaDTO.setValorLimite(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, valorLimite)
                ? converterParaDouble(valorLimite, 2)
                : null);
        contaDTO.setDiaVencimento(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, diaVencimento)
                ? converterParaInteger(diaVencimento)
                : null);
        contaDTO.setNumeroConta(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, numeroConta)
                ? converterParaInteger(numeroConta)
                : null);

        return contaDTO;
    }

}
