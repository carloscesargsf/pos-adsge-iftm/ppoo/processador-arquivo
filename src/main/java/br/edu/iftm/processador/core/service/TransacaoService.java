package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.TransacaoDTO;
import br.edu.iftm.processador.core.entity.Transacao;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import br.edu.iftm.processador.repository.TransacaoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static br.edu.iftm.processador.constants.ConstantesGerais.REGISTRO_JA_EXISTE;

public class TransacaoService implements CreateService<TransacaoDTO> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Override
    public void salvarRegistro(TransacaoDTO registro) {
        create(registro);
    }

    @Transactional
    public TransacaoDTO create(TransacaoDTO registro) {
        logger.debug("Requisição para cadastrar {} : {}", registro.getClass().getName(), registro);

        if (registro.getNumeroTransacao() != null && transacaoRepository.findByNumeroTransacao(registro.getNumeroTransacao()).isPresent()) {
            throw new ProcessadorDeArquivosException(REGISTRO_JA_EXISTE);
        }

        return new TransacaoDTO(
                transacaoRepository.save(new Transacao(registro)));
    }

}
