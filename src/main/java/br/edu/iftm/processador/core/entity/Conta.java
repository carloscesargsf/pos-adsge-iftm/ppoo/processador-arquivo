package br.edu.iftm.processador.core.entity;

import br.edu.iftm.processador.core.dto.ContaDTO;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import com.sun.istack.internal.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

import static br.edu.iftm.processador.constants.ConstantesGerais.ERRO_AO_MAPEAR_ENTIDADE;

@Entity
@Table(name = "tbConta")
public class Conta {

    @Id
    @Column(name = "nrocta", length = 7, updatable = false)
    private Integer numeroConta;

    @NotNull
    @Column(name = "cpfcli", length = 11)
    private String cpf;

    @NotNull
    @Column(name = "vlrlim", length = 10, precision = 2)
    private Double valorLimite;

    @NotNull
    @Column(name = "diaven", length = 2)
    private Integer diaVencimento;

    public Conta() {
    }

    public Conta(ContaDTO contaDTO) {
        if (getNumeroConta() == null) {
            setNumeroConta(contaDTO.getNumeroConta());
        } else {
            if (!getNumeroConta().equals(contaDTO.getNumeroConta())) {
                throw new ProcessadorDeArquivosException(ERRO_AO_MAPEAR_ENTIDADE);
            }
        }

        setCpf(contaDTO.getCpf());
        setValorLimite(contaDTO.getValorLimite());
        setDiaVencimento(contaDTO.getDiaVencimento());
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getValorLimite() {
        return valorLimite;
    }

    public void setValorLimite(Double valorLimite) {
        this.valorLimite = valorLimite;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Conta that = (Conta) o;
        return cpf.equals(that.cpf);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cpf);
    }

    @Override
    public String toString() {
        return "Conta{" +
                "numeroConta=" + numeroConta +
                ", cpf='" + cpf + '\'' +
                ", valorLimite=" + valorLimite +
                ", diaVencimento=" + diaVencimento +
                '}';
    }

}
