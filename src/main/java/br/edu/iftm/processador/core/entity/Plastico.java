package br.edu.iftm.processador.core.entity;

import br.edu.iftm.processador.core.dto.PlasticoDTO;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import com.sun.istack.internal.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

import static br.edu.iftm.processador.constants.ConstantesGerais.ERRO_AO_MAPEAR_ENTIDADE;

@Entity
@Table(name = "tbPlastico")
public class Plastico {

    @Id
    @Column(name = "nropla", length = 7, updatable = false)
    private Integer numeroPlastico;

    @NotNull
    @Column(name = "nrocta", length = 7)
    private Integer numeroConta;

    @NotNull
    @Column(name = "nompla", length = 30)
    private String nomePlastico;

    @NotNull
    @Column(name = "cpfpla", length = 11)
    private String cpfPlastico;

    public Plastico() {
    }

    public Plastico(PlasticoDTO plasticoDTO) {
        if (getNumeroPlastico() == null) {
            setNumeroPlastico(plasticoDTO.getNumeroPlastico());
        } else {
            if (!getNumeroPlastico().equals(plasticoDTO.getNumeroPlastico())) {
                throw new ProcessadorDeArquivosException(ERRO_AO_MAPEAR_ENTIDADE);
            }
        }

        setNumeroConta(plasticoDTO.getNumeroConta());
        setNomePlastico(plasticoDTO.getNomePlastico());
        setCpfPlastico(plasticoDTO.getCpfPlastico());
    }

    public Integer getNumeroPlastico() {
        return numeroPlastico;
    }

    public void setNumeroPlastico(Integer numeroPlastico) {
        this.numeroPlastico = numeroPlastico;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getNomePlastico() {
        return nomePlastico;
    }

    public void setNomePlastico(String nomePlastico) {
        this.nomePlastico = nomePlastico;
    }

    public String getCpfPlastico() {
        return cpfPlastico;
    }

    public void setCpfPlastico(String cpfPlastico) {
        this.cpfPlastico = cpfPlastico;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plastico that = (Plastico) o;
        return numeroPlastico.equals(that.numeroPlastico);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numeroPlastico);
    }

    @Override
    public String toString() {
        return "Plastico{" +
                "numeroPlastico=" + numeroPlastico +
                ", numeroConta=" + numeroConta +
                ", nomePlastico='" + nomePlastico + '\'' +
                ", cpfPlastico='" + cpfPlastico + '\'' +
                '}';
    }

}
