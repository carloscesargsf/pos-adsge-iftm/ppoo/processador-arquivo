package br.edu.iftm.processador.core.entity;

import br.edu.iftm.processador.core.dto.TransacaoDTO;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

import static br.edu.iftm.processador.constants.ConstantesGerais.ERRO_AO_MAPEAR_ENTIDADE;

@Entity
@Table(name = "tbTransacao")
public class Transacao {

    @Id
    @Column(name = "nrotra", length = 11, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer numeroTransacao;

    @NotNull
    @Column(name = "nrocta", length = 7)
    private Integer numeroConta;

    @NotNull
    @Column(name = "nropla", length = 7)
    private Integer numeroPlastico;

    @NotNull
    @Column(name = "vlrtra", length = 10, precision = 2)
    private Double valor;

    @NotNull
    @Column(name = "dattra")
    private LocalDateTime dataTransacao;

    @NotNull
    @Column(name = "codloj", length = 7)
    private Integer codigoLoja;

    public Transacao() {
    }

    public Transacao(TransacaoDTO transacaoDTO) {
        if (getNumeroTransacao() == null) {
            setNumeroTransacao(transacaoDTO.getNumeroTransacao());
        } else {
            if (!getNumeroTransacao().equals(transacaoDTO.getNumeroTransacao())) {
                throw new ProcessadorDeArquivosException(ERRO_AO_MAPEAR_ENTIDADE);
            }
        }

        setNumeroConta(transacaoDTO.getNumeroConta());
        setNumeroPlastico(transacaoDTO.getNumeroPlastico());
        setValor(transacaoDTO.getValor());
        setDataTransacao(transacaoDTO.getDataTransacao());
        setCodigoLoja(transacaoDTO.getCodigoLoja());
    }

    public Integer getNumeroTransacao() {
        return numeroTransacao;
    }

    public void setNumeroTransacao(Integer numeroTransacao) {
        this.numeroTransacao = numeroTransacao;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Integer getNumeroPlastico() {
        return numeroPlastico;
    }

    public void setNumeroPlastico(Integer numeroPlastico) {
        this.numeroPlastico = numeroPlastico;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public LocalDateTime getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(LocalDateTime dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    public Integer getCodigoLoja() {
        return codigoLoja;
    }

    public void setCodigoLoja(Integer codigoLoja) {
        this.codigoLoja = codigoLoja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transacao that = (Transacao) o;
        return numeroTransacao.equals(that.numeroTransacao);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numeroTransacao);
    }

    @Override
    public String toString() {
        return "Transacao{" +
                "numeroTransacao=" + numeroTransacao +
                ", numeroConta=" + numeroConta +
                ", numeroPlastico=" + numeroPlastico +
                ", valor=" + valor +
                ", dataTransacao=" + dataTransacao +
                ", codigoLoja=" + codigoLoja +
                '}';
    }

}
