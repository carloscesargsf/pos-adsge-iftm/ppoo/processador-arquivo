package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.DefaultDTO;

public interface DefaultService<E extends DefaultDTO> {

    void salvarRegistro(E registro);

}
