package br.edu.iftm.processador.core.dto;

import br.edu.iftm.processador.core.entity.Conta;

public class ContaDTO implements DefaultDTO {

    private String acao;

    private String cpf;

    private Double valorLimite;

    private Integer diaVencimento;

    private Integer numeroConta;

    public ContaDTO() {
    }

    public ContaDTO(Conta conta) {
        setCpf(conta.getCpf());
        setValorLimite(conta.getValorLimite());
        setDiaVencimento(conta.getDiaVencimento());
        setNumeroConta(conta.getNumeroConta());
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getValorLimite() {
        return valorLimite;
    }

    public void setValorLimite(Double valorLimite) {
        this.valorLimite = valorLimite;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        ContaDTO that = (ContaDTO) o;
//        return cpf.equals(that.cpf);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(cpf);
//    }
//
//    @Override
//    public String toString() {
//        return "ContaDTO{" +
//                "acao='" + acao + '\'' +
//                ", numeroConta=" + numeroConta +
//                ", cpf='" + cpf + '\'' +
//                ", valorLimite=" + valorLimite +
//                ", diaVencimento=" + diaVencimento +
//                '}';
//    }

}
