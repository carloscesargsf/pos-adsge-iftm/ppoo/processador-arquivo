package br.edu.iftm.processador.core.dto;

import br.edu.iftm.processador.core.entity.ControleRecebimento;

import java.time.LocalDateTime;
import java.util.Objects;

public class ControleRecebimentoDTO implements DefaultDTO {

    private Integer id;

    private String tipoArquivo;

    private Integer numeroLote;

    private LocalDateTime dtProcessamento;

    public ControleRecebimentoDTO() {
    }

    public ControleRecebimentoDTO(ControleRecebimento controleRecebimento) {
        setId(controleRecebimento.getId());
        setTipoArquivo(controleRecebimento.getTipoArquivo());
        setNumeroLote(controleRecebimento.getNumeroLote());
        setDtProcessamento(controleRecebimento.getDtProcessamento());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(String tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public Integer getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(Integer numeroLote) {
        this.numeroLote = numeroLote;
    }

    public LocalDateTime getDtProcessamento() {
        return dtProcessamento;
    }

    public void setDtProcessamento(LocalDateTime dtProcessamento) {
        this.dtProcessamento = dtProcessamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ControleRecebimentoDTO that = (ControleRecebimentoDTO) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ControleRecebimentoDTO{" +
                "id=" + id +
                ", tipoArquivo='" + tipoArquivo + '\'' +
                ", numeroLote='" + numeroLote + '\'' +
                ", dtProcessamento=" + dtProcessamento +
                '}';
    }

}
