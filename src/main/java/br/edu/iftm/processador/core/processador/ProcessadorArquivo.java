package br.edu.iftm.processador.core.processador;

import java.util.List;

public interface ProcessadorArquivo {

    void processaArquivo(List<String> linhas);

}
