package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.dto.ControleRecebimentoDTO;
import br.edu.iftm.processador.core.dto.DefaultDTO;
import br.edu.iftm.processador.core.service.ControleRecebimentoService;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static br.edu.iftm.processador.constants.ConstantesGerais.LOTE_JA_PROCESSADO;
import static br.edu.iftm.processador.constants.ConstantesGerais.LOTE_SEM_PROCESSAR;
import static br.edu.iftm.processador.utils.FormatterUtils.converterParaInteger;

public class ProcessadorTipoRegistroCabecalho extends ProcessadorTipoRegistro {

    private String tipoArquivo;

    @Autowired
    private ControleRecebimentoService controleRecebimentoService;

    public ProcessadorTipoRegistroCabecalho(String tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    @Override
    public DefaultDTO processaLinha(String linha) {
        Integer numeroLoteAtual = converterParaInteger(linha.substring(2, 5));

        Integer proximoLote = controleRecebimentoService
                .buscaUltimoLoteProcessado(tipoArquivo)
                .getNumeroLote() + 1;

        if (proximoLote > numeroLoteAtual) {
            throw new ProcessadorDeArquivosException(String.format(LOTE_JA_PROCESSADO,
                    numeroLoteAtual, tipoArquivo));
        }

        if (numeroLoteAtual > proximoLote) {
            List<Integer> lotesSemProcessar = new ArrayList<>();
            for (int i = proximoLote; i < numeroLoteAtual; i++) {
                lotesSemProcessar.add(i);
            }

            throw new ProcessadorDeArquivosException(String.format(
                    LOTE_SEM_PROCESSAR, lotesSemProcessar.toString()));
        }

        ControleRecebimentoDTO controleRecebimentoDTO = new ControleRecebimentoDTO();
        controleRecebimentoDTO.setTipoArquivo(tipoArquivo);
        controleRecebimentoDTO.setNumeroLote(numeroLoteAtual);
        controleRecebimentoDTO.setDtProcessamento(LocalDateTime.now());

        return controleRecebimentoDTO;
    }
}
