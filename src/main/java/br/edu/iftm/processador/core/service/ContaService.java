package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.ContaDTO;
import br.edu.iftm.processador.core.entity.Conta;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import br.edu.iftm.processador.repository.ContaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static br.edu.iftm.processador.constants.ConstantesGerais.*;

public class ContaService implements CreateUpdateService<ContaDTO> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ContaRepository contaRepository;

    @Override
    public void salvarRegistro(ContaDTO registro) {
        if (ACAO_ALTERACAO.equals(registro.getAcao())) {
            update(registro);
        } else if (ACAO_INCLUSAO.equals(registro.getAcao())) {
            create(registro);
        } else {
            throw new ProcessadorDeArquivosException("Ação não implementada!");
        }
    }

    @Transactional
    public ContaDTO create(ContaDTO registro) {
        logger.debug("Requisição para cadastrar {} : {}", registro.getClass().getName(), registro);

        if (registro.getNumeroConta() != null
                && contaRepository.findByNumeroConta(registro.getNumeroConta()).isPresent()) {
            throw new ProcessadorDeArquivosException(REGISTRO_JA_EXISTE);
        }

        return new ContaDTO(
                contaRepository.save(new Conta(registro)));
    }

    @Transactional
    public ContaDTO update(ContaDTO registro) {
        logger.debug("Requisição para atualizar {} : {}", contaRepository.getClass().getName(), registro);

        if (registro.getCpf() == null) {
            throw new ProcessadorDeArquivosException(REGISTRO_NAO_ENCONTRADO);
        }

        Conta conta = findOneOrElseThrow(registro.getNumeroConta());
        conta.setCpf(registro.getCpf());
        conta.setValorLimite(registro.getValorLimite());
        if (null != registro.getDiaVencimento()) {
            conta.setDiaVencimento(registro.getDiaVencimento());
        }

        return new ContaDTO(contaRepository.save(conta));
    }

    private Conta findOneOrElseThrow(Integer numeroConta) {
        logger.debug("Requisição para buscar {}: {}", contaRepository.getClass().getName(), numeroConta);
        return contaRepository.findByNumeroConta(numeroConta)
                .orElseThrow(() -> new ProcessadorDeArquivosException(REGISTRO_NAO_ENCONTRADO));
    }

}
