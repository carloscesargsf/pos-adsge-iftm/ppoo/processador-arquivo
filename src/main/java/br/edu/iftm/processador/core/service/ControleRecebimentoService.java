package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.ControleRecebimentoDTO;
import br.edu.iftm.processador.core.entity.ControleRecebimento;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import br.edu.iftm.processador.repository.ControleRecebimentoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static br.edu.iftm.processador.constants.ConstantesGerais.*;

@Service
public class ControleRecebimentoService implements CreateService<ControleRecebimentoDTO> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    ControleRecebimentoRepository controleRecebimentoRepository;

    @Transactional
    public ControleRecebimentoDTO create(ControleRecebimentoDTO registro) {
        logger.debug("Requisição para cadastrar {} : {}", registro.getClass().getName(),
                registro);

        if (registro.getId() != null && controleRecebimentoRepository.findById(registro.getId()).isPresent()) {
            throw new ProcessadorDeArquivosException(REGISTRO_JA_EXISTE);
        }

        return new ControleRecebimentoDTO(
                controleRecebimentoRepository.save(new ControleRecebimento(registro)));
    }

    public ControleRecebimentoDTO buscaUltimoLoteProcessado(String tipoArquivo) {
        return controleRecebimentoRepository.queryFirst1ByTipoArquivoOrderByNumeroLoteDesc(tipoArquivo)
                .map(ControleRecebimentoDTO::new).orElse(null);
    }

    @Override
    public void salvarRegistro(ControleRecebimentoDTO registro) {
        Integer proximoLote = buscaUltimoLoteProcessado(registro.getTipoArquivo())
                .getNumeroLote() + 1;

        if (proximoLote > registro.getNumeroLote()) {
            throw new ProcessadorDeArquivosException(String.format(LOTE_JA_PROCESSADO,
                    registro.getNumeroLote(), registro.getTipoArquivo()));
        }

        if (registro.getNumeroLote() > proximoLote) {
            List<Integer> lotesSemProcessar = new ArrayList<>();
            for (int i = proximoLote; i < registro.getNumeroLote(); i++) {
                lotesSemProcessar.add(i);
            }

            throw new ProcessadorDeArquivosException(String.format(
                    LOTE_SEM_PROCESSAR, lotesSemProcessar.toString()));
        }

        create(registro);
    }

}
