package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.PlasticoDTO;
import br.edu.iftm.processador.core.entity.Plastico;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import br.edu.iftm.processador.repository.PlasticoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static br.edu.iftm.processador.constants.ConstantesGerais.REGISTRO_JA_EXISTE;

public class PlasticoService implements CreateService<PlasticoDTO> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PlasticoRepository plasticoRepository;

    @Override
    public void salvarRegistro(PlasticoDTO registro) {
        create(registro);
    }

    @Transactional
    public PlasticoDTO create(PlasticoDTO registro) {
        logger.debug("Requisição para cadastrar {} : {}", registro.getClass().getName(), registro);

        if (registro.getNumeroPlastico() != null
                && plasticoRepository.findByNumeroPlastico(registro.getNumeroPlastico()).isPresent()) {
            throw new ProcessadorDeArquivosException(REGISTRO_JA_EXISTE);
        }

        return new PlasticoDTO(
                plasticoRepository.save(new Plastico(registro)));
    }

}
