package br.edu.iftm.processador.core.dto;

import br.edu.iftm.processador.core.entity.Cliente;

import java.time.LocalDateTime;

public class ClienteDTO implements DefaultDTO {

    private String acao;

    private String cpf;

    private String nome;

    private String endereco;

    private String bairro;

    private String cidade;

    private String estado;

    private LocalDateTime dataCadastro;

    public ClienteDTO() {
    }

    public ClienteDTO(Cliente cliente) {
        setCpf(cliente.getCpf());
        setNome(cliente.getNome());
        setEndereco(cliente.getEndereco());
        setBairro(cliente.getBairro());
        setCidade(cliente.getCidade());
        setEstado(cliente.getEstado());
        setDataCadastro(cliente.getDataCadastro());
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        ClienteDTO that = (ClienteDTO) o;
//        return cpf.equals(that.cpf);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(cpf);
//    }
//
//    @Override
//    public String toString() {
//        return "ClienteDTO{" +
//                "cpf='" + cpf + '\'' +
//                ", nome='" + nome + '\'' +
//                ", endereco='" + endereco + '\'' +
//                ", bairro='" + bairro + '\'' +
//                ", cidade='" + cidade + '\'' +
//                ", estado='" + estado + '\'' +
//                ", dataCadastro=" + dataCadastro +
//                '}';
//    }

}
