package br.edu.iftm.processador.core.processador.arquivoTransacoes;

import br.edu.iftm.processador.core.service.ControleRecebimentoService;
import br.edu.iftm.processador.core.service.DefaultService;
import br.edu.iftm.processador.core.service.TransacaoService;
import br.edu.iftm.processador.core.tipoRegistro.ProcessadorTipoRegistro;
import br.edu.iftm.processador.core.tipoRegistro.ProcessadorTipoRegistroArquivoTransacoesDetalhe;
import br.edu.iftm.processador.core.tipoRegistro.ProcessadorTipoRegistroCabecalho;
import br.edu.iftm.processador.enums.ArquivoProcessamento;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;

import java.util.Map;
import java.util.TreeMap;

public enum TipoRegistro {

    CABECALHO(1, new ProcessadorTipoRegistroCabecalho(ArquivoProcessamento.ARQUIVO_TRANSACOES.getTipoArquivo()),
            ControleRecebimentoService.class),
    DETALHE(2, new ProcessadorTipoRegistroArquivoTransacoesDetalhe(),
            TransacaoService.class);

    private final Integer tipo;

    private final ProcessadorTipoRegistro processadorTipoRegistro;

    private Class<DefaultService> service;

    private static Map<Integer, TipoRegistro> tiposRegistros = new TreeMap<>();

    TipoRegistro(Integer tipo, ProcessadorTipoRegistro processadorTipoRegistro, Class service) {
        this.tipo = tipo;
        this.processadorTipoRegistro = processadorTipoRegistro;
        this.service = service;
    }

    public Integer getTipo() {
        return tipo;
    }

    public ProcessadorTipoRegistro getProcessadorTipoRegistro() {
        return processadorTipoRegistro;
    }

    public Class<DefaultService> getService() {
        return service;
    }

    static {
        for (TipoRegistro tipoRegistro : TipoRegistro.values()) {
            if (tiposRegistros.containsKey(tipoRegistro.getTipo())) {
                throw new ProcessadorDeArquivosException("");
            }

            tiposRegistros.put(tipoRegistro.getTipo(), tipoRegistro);
        }
    }

    public static TipoRegistro getTipoRegistroByTipo(String tipo) {
        return tiposRegistros.get(Integer.parseInt(tipo));
    }

}
