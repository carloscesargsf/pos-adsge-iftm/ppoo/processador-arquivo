package br.edu.iftm.processador.core.processador.arquivoContas;

import br.edu.iftm.processador.core.BeanProvider;
import br.edu.iftm.processador.core.dto.DefaultDTO;
import br.edu.iftm.processador.core.processador.ProcessadorArquivo;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;

import java.lang.reflect.Method;
import java.util.List;

public class ProcessadorArquivoContas implements ProcessadorArquivo {

    @Override
    public void processaArquivo(List<String> linhas) {
        for (String linha : linhas) {
            TipoRegistro tipoRegistro = TipoRegistro.getTipoRegistroByTipo(
                    String.valueOf(linha.charAt(1)));

            try {
                DefaultDTO registro = tipoRegistro.getProcessadorTipoRegistro().processaLinha(linha);
                Object service = tipoRegistro.getService().newInstance();
                BeanProvider.autowire(service);
                Method method = service.getClass().getMethod("salvarRegistro", registro.getClass());
                method.invoke(service, registro);
            } catch (Exception e) {
                throw new ProcessadorDeArquivosException(e.getMessage());
            }
        }
    }

}
