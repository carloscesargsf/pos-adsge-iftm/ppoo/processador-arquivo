package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.dto.ClienteDTO;
import br.edu.iftm.processador.core.dto.DefaultDTO;

import java.util.regex.Pattern;

import static br.edu.iftm.processador.constants.ConstantesGerais.ONLY_UNDERSCORE_STRING_PATTERN;
import static br.edu.iftm.processador.constants.ConstantesGerais.PT_BR_DATETIME_FORMATTER_ONLY_NUMBERS;
import static br.edu.iftm.processador.utils.FormatterUtils.converterParaLocalDateTime;

public class ProcessadorTipoRegistroArquivoClientesDetalhe extends ProcessadorTipoRegistro {

    public DefaultDTO processaLinha(String linha) {
        String acao = String.valueOf(linha.charAt(2));
        String cpf = linha.substring(3, 14).trim();
        String nome = linha.substring(14, 44).trim();
        String endereco = linha.substring(44, 74).trim();
        String bairro = linha.substring(74, 104).trim();
        String cidade = linha.substring(104, 134).trim();
        String estado = linha.substring(134, 136).trim();
        String dataCadastro = linha.substring(136, 150);

        ClienteDTO clienteDTO = new ClienteDTO();

        clienteDTO.setAcao(acao);
        clienteDTO.setCpf(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, cpf)
                ? cpf
                : "");
        clienteDTO.setNome(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, nome)
                ? nome
                : "");
        clienteDTO.setEndereco(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, endereco)
                ? endereco
                : "");
        clienteDTO.setBairro(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, bairro)
                ? bairro
                : "");
        clienteDTO.setCidade(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, cidade)
                ? cidade
                : "");
        clienteDTO.setEstado(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, estado)
                ? estado
                : "");
        if (!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, dataCadastro)) {
            clienteDTO.setDataCadastro(converterParaLocalDateTime(
                    dataCadastro, PT_BR_DATETIME_FORMATTER_ONLY_NUMBERS));
        }

        return clienteDTO;
    }

}
