package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.BeanProvider;
import br.edu.iftm.processador.core.dto.DefaultDTO;

public abstract class ProcessadorTipoRegistro {

    public ProcessadorTipoRegistro() {
        BeanProvider.autowire(this);
    }

    public abstract DefaultDTO processaLinha(String linha);

}
