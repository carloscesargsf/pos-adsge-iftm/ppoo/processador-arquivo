package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.DefaultDTO;

public interface CreateUpdateService<E extends DefaultDTO> extends CreateService<E> {

    E update(E registro);

}
