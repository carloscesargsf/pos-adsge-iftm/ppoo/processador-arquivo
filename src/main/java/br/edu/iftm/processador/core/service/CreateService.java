package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.DefaultDTO;

public interface CreateService<E extends DefaultDTO> extends DefaultService<E> {

    E create(E registro);

}
