package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.dto.DefaultDTO;
import br.edu.iftm.processador.core.dto.TransacaoDTO;

import java.util.regex.Pattern;

import static br.edu.iftm.processador.constants.ConstantesGerais.ONLY_UNDERSCORE_STRING_PATTERN;
import static br.edu.iftm.processador.constants.ConstantesGerais.PT_BR_DATETIME_FORMATTER_ONLY_NUMBERS;
import static br.edu.iftm.processador.utils.FormatterUtils.*;

public class ProcessadorTipoRegistroArquivoTransacoesDetalhe extends ProcessadorTipoRegistro {

    public DefaultDTO processaLinha(String linha) {
        String numeroConta = linha.substring(2, 9).trim();
        String numeroPlastico = linha.substring(9, 16).trim();
        String valor = linha.substring(16, 28).trim();
        String dataTransacao = linha.substring(28, 42).trim();
        String codigoLoja = linha.substring(42, 48).trim();

        TransacaoDTO transacaoDTO = new TransacaoDTO();

        transacaoDTO.setNumeroConta(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, numeroConta)
                ? converterParaInteger(numeroConta)
                : null);
        transacaoDTO.setNumeroPlastico(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, numeroPlastico)
                ? converterParaInteger(numeroPlastico)
                : null);
        transacaoDTO.setValor(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, valor)
                ? converterParaDouble(valor, 2)
                : null);
        transacaoDTO.setCodigoLoja(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, codigoLoja)
                ? converterParaInteger(codigoLoja)
                : null);
        if (!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, dataTransacao)) {
            transacaoDTO.setDataTransacao(converterParaLocalDateTime(
                    dataTransacao, PT_BR_DATETIME_FORMATTER_ONLY_NUMBERS));
        }

        return transacaoDTO;
    }

}
