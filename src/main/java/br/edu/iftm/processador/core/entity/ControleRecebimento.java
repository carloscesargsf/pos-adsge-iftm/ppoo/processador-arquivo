package br.edu.iftm.processador.core.entity;

import br.edu.iftm.processador.core.dto.ControleRecebimentoDTO;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

import static br.edu.iftm.processador.constants.ConstantesGerais.ERRO_AO_MAPEAR_ENTIDADE;

@Entity
@Table(name = "tbControleRecebimento")
public class ControleRecebimento {

    @Id
    @Column(name = "codconrec", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "tiparq", length = 3)
    private String tipoArquivo;

    @NotNull
    @Column(name = "numlot", length = 7)
    private Integer numeroLote;

    @Column(name = "dathraprc")
    private LocalDateTime dtProcessamento;

    public ControleRecebimento() {
    }

    public ControleRecebimento(ControleRecebimentoDTO controleRecebimentoDTO) {
        if (getId() == null) {
            setId(controleRecebimentoDTO.getId());
        } else {
            if (!getId().equals(controleRecebimentoDTO.getId())) {
                throw new ProcessadorDeArquivosException(ERRO_AO_MAPEAR_ENTIDADE);
            }
        }

        setTipoArquivo(controleRecebimentoDTO.getTipoArquivo());
        setNumeroLote(controleRecebimentoDTO.getNumeroLote());
        setDtProcessamento(controleRecebimentoDTO.getDtProcessamento());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(String tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public Integer getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(Integer numeroLote) {
        this.numeroLote = numeroLote;
    }

    public LocalDateTime getDtProcessamento() {
        return dtProcessamento;
    }

    public void setDtProcessamento(LocalDateTime dtProcessamento) {
        this.dtProcessamento = dtProcessamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ControleRecebimento that = (ControleRecebimento) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ControleRecebimento{" +
                "id=" + id +
                ", tipoArquivo='" + tipoArquivo + '\'' +
                ", numeroLote='" + numeroLote + '\'' +
                ", dtProcessamento=" + dtProcessamento +
                '}';
    }

}
