package br.edu.iftm.processador.core.service;

import br.edu.iftm.processador.core.dto.ClienteDTO;
import br.edu.iftm.processador.core.entity.Cliente;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import br.edu.iftm.processador.repository.ClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static br.edu.iftm.processador.constants.ConstantesGerais.*;

@Service
public class ClienteService implements CreateUpdateService<ClienteDTO> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public void salvarRegistro(ClienteDTO registro) {
        if (ACAO_ALTERACAO.equals(registro.getAcao())) {
            update(registro);
        } else if (ACAO_INCLUSAO.equals(registro.getAcao())) {
            create(registro);
        } else {
            throw new ProcessadorDeArquivosException("Ação não implementada!");
        }
    }

    @Transactional
    public ClienteDTO create(ClienteDTO registro) {
        logger.debug("Requisição para cadastrar {} : {}", registro.getClass().getName(), registro);

        if (registro.getCpf() != null
                && clienteRepository.findByCpf(registro.getCpf()).isPresent()) {
            throw new ProcessadorDeArquivosException(REGISTRO_JA_EXISTE);
        }

        return new ClienteDTO(
                clienteRepository.save(new Cliente(registro)));
    }

    @Transactional
    public ClienteDTO update(ClienteDTO registro) {
        logger.debug("Requisição para atualizar {} : {}", clienteRepository.getClass().getName(), registro);

        if (registro.getCpf() == null) {
            throw new ProcessadorDeArquivosException(REGISTRO_NAO_ENCONTRADO);
        }

        Cliente cliente = findOneOrElseThrow(registro.getCpf());
        cliente.setNome(registro.getNome());
        cliente.setEndereco(registro.getEndereco());
        cliente.setBairro(registro.getBairro());
        cliente.setCidade(registro.getCidade());
        cliente.setEstado(registro.getEstado());
        cliente.setDataCadastro(registro.getDataCadastro());

        return new ClienteDTO(clienteRepository.save(cliente));
    }

    private Cliente findOneOrElseThrow(String cpf) {
        logger.debug("Requisição para buscar {}: {}", clienteRepository.getClass().getName(), cpf);
        return clienteRepository.findByCpf(cpf)
                .orElseThrow(() -> new ProcessadorDeArquivosException(REGISTRO_NAO_ENCONTRADO));
    }

}
