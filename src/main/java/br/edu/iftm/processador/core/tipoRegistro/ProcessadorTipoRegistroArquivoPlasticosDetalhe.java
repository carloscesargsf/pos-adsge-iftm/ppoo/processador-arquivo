package br.edu.iftm.processador.core.tipoRegistro;

import br.edu.iftm.processador.core.dto.DefaultDTO;
import br.edu.iftm.processador.core.dto.PlasticoDTO;

import java.util.regex.Pattern;

import static br.edu.iftm.processador.constants.ConstantesGerais.ONLY_UNDERSCORE_STRING_PATTERN;
import static br.edu.iftm.processador.utils.FormatterUtils.converterParaInteger;

public class ProcessadorTipoRegistroArquivoPlasticosDetalhe extends ProcessadorTipoRegistro {

    public DefaultDTO processaLinha(String linha) {
        String numeroConta = linha.substring(2, 9).trim();
        String nomePlastico = linha.substring(9, 39).trim();
        String cpfPlastico = linha.substring(39, 50).trim();
        String numeroPlastico = linha.substring(50, 57).trim();

        PlasticoDTO plasticoDTO = new PlasticoDTO();

        plasticoDTO.setNumeroConta(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, numeroConta)
                ? converterParaInteger(numeroConta)
                : null);
        plasticoDTO.setNomePlastico(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, nomePlastico)
                ? nomePlastico
                : "");
        plasticoDTO.setCpfPlastico(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, cpfPlastico)
                ? cpfPlastico
                : "");
        plasticoDTO.setNumeroPlastico(!Pattern.matches(ONLY_UNDERSCORE_STRING_PATTERN, numeroPlastico)
                ? converterParaInteger(numeroPlastico)
                : null);

        return plasticoDTO;
    }

}
