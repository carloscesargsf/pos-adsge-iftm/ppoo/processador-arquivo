package br.edu.iftm.processador.core.entity;

import br.edu.iftm.processador.core.dto.ClienteDTO;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import com.sun.istack.internal.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

import static br.edu.iftm.processador.constants.ConstantesGerais.ERRO_AO_MAPEAR_ENTIDADE;

@Entity
@Table(name = "tbCliente")
public class Cliente {

    @Id
    @Column(name = "cpfcli", length = 11, updatable = false)
    private String cpf;

    @NotNull
    @Column(name = "nomcli", length = 30)
    private String nome;

    @NotNull
    @Column(name = "endcli", length = 30)
    private String endereco;

    @NotNull
    @Column(name = "baicli", length = 30)
    private String bairro;

    @NotNull
    @Column(name = "cidcli", length = 30)
    private String cidade;

    @NotNull
    @Column(name = "sigest", length = 2)
    private String estado;

    @Column(name = "datcad")
    private LocalDateTime dataCadastro;

    public Cliente() {
    }

    public Cliente(ClienteDTO clienteDTO) {
        if (getCpf() == null) {
            setCpf(clienteDTO.getCpf());
        } else {
            if (!getCpf().equals(clienteDTO.getCpf())) {
                throw new ProcessadorDeArquivosException(ERRO_AO_MAPEAR_ENTIDADE);
            }
        }

        setNome(clienteDTO.getNome());
        setEndereco(clienteDTO.getEndereco());
        setBairro(clienteDTO.getBairro());
        setCidade(clienteDTO.getCidade());
        setEstado(clienteDTO.getEstado());
        setDataCadastro(clienteDTO.getDataCadastro());
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LocalDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente that = (Cliente) o;
        return cpf.equals(that.cpf);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cpf);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "cpf='" + cpf + '\'' +
                ", nome='" + nome + '\'' +
                ", endereco='" + endereco + '\'' +
                ", bairro='" + bairro + '\'' +
                ", cidade='" + cidade + '\'' +
                ", estado='" + estado + '\'' +
                ", dataCadastro=" + dataCadastro +
                '}';
    }
}
