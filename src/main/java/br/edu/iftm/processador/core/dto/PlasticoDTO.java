package br.edu.iftm.processador.core.dto;

import br.edu.iftm.processador.core.entity.Plastico;

public class PlasticoDTO implements DefaultDTO {

    private Integer numeroConta;

    private String nomePlastico;

    private String cpfPlastico;

    private Integer numeroPlastico;

    public PlasticoDTO() {
    }

    public PlasticoDTO(Plastico plastico) {
        setNumeroPlastico(plastico.getNumeroPlastico());
        setNumeroConta(plastico.getNumeroConta());
        setNomePlastico(plastico.getNomePlastico());
        setCpfPlastico(plastico.getCpfPlastico());
    }

    public Integer getNumeroPlastico() {
        return numeroPlastico;
    }

    public void setNumeroPlastico(Integer numeroPlastico) {
        this.numeroPlastico = numeroPlastico;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getNomePlastico() {
        return nomePlastico;
    }

    public void setNomePlastico(String nomePlastico) {
        this.nomePlastico = nomePlastico;
    }

    public String getCpfPlastico() {
        return cpfPlastico;
    }

    public void setCpfPlastico(String cpfPlastico) {
        this.cpfPlastico = cpfPlastico;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        PlasticoDTO that = (PlasticoDTO) o;
//        return numeroPlastico.equals(that.numeroPlastico);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(numeroPlastico);
//    }
//
//    @Override
//    public String toString() {
//        return "Plastico{" +
//                "numeroPlastico=" + numeroPlastico +
//                ", numeroConta=" + numeroConta +
//                ", nomePlastico='" + nomePlastico + '\'' +
//                ", cpfPlastico='" + cpfPlastico + '\'' +
//                '}';
//    }

}
