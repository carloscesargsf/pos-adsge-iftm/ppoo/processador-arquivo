package br.edu.iftm.processador;

import br.edu.iftm.processador.core.processador.ProcessadorArquivo;
import br.edu.iftm.processador.enums.ArquivoProcessamento;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static br.edu.iftm.processador.constants.ConstantesGerais.CAMINHO_BASE_ARQUIVOS_A_PROCESSAR;
import static br.edu.iftm.processador.constants.ConstantesGerais.CAMINHO_BASE_ARQUIVOS_PROCESSADOS;
import static br.edu.iftm.processador.service.LeituraArquivoService.lerArquivo;
import static br.edu.iftm.processador.service.LogService.imprimeLog;

@SpringBootApplication
@EntityScan(basePackageClasses = {ProcessadorDeArquivosApplication.class, Jsr310JpaConverters.class})
public class ProcessadorDeArquivosApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ProcessadorDeArquivosApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        imprimeLog("Sistema Processador de Arquivos");
        imprimeLog("Início do Processamento");

        for (Map.Entry<Integer, ArquivoProcessamento> arquivoProcessamento
                : ArquivoProcessamento.getArquivosProcessamento().entrySet()) {
            File[] arquivos = new File(CAMINHO_BASE_ARQUIVOS_A_PROCESSAR).listFiles(
                    (diretorio, nomeArquivo) -> nomeArquivo
                            .endsWith(String.format(arquivoProcessamento.getValue().getPadraoNomeArquivo(), "")));
            for (File arquivo : arquivos) {
                processa(arquivoProcessamento.getValue(), arquivo);
            }
        }

        imprimeLog("Fim do Processamento");
    }

    private void processa(ArquivoProcessamento arquivoProcessamento, File arquivo) {
        imprimeLog("Início do Processamento do arquivo " + arquivo.getName());

        try {
            String[] splittedNomeArquivo = arquivo.getName().split("_", 2);

            if ((arquivoProcessamento.getOrdemExecucao().equals(1)) ||
                    (null != ArquivoProcessamento.getArquivosProcessamento().get(arquivoProcessamento.getOrdemExecucao() - 1) &&
                            (new File(CAMINHO_BASE_ARQUIVOS_PROCESSADOS +
                                    String.format(ArquivoProcessamento.getArquivosProcessamento().get(arquivoProcessamento.getOrdemExecucao() - 1)
                                            .getPadraoNomeArquivo(), splittedNomeArquivo[0]))).exists())) {
                String arquivoAProcessar = CAMINHO_BASE_ARQUIVOS_A_PROCESSAR + arquivo.getName();
                String arquivoProcessado = CAMINHO_BASE_ARQUIVOS_PROCESSADOS + arquivo.getName();

                ProcessadorArquivo processadorArquivo = arquivoProcessamento.getProcessadorArquivo();
                processadorArquivo.processaArquivo(lerArquivo(arquivoAProcessar));

                Files.move(Paths.get(arquivoAProcessar), Paths.get(arquivoProcessado));
            } else {
                imprimeLog("Arquivo " + String.format(ArquivoProcessamento.getArquivosProcessamento()
                        .get(arquivoProcessamento.getOrdemExecucao() - 1)
                        .getPadraoNomeArquivo(), splittedNomeArquivo[0]) + " não foi processado!");
            }
        } catch (ProcessadorDeArquivosException e) {
            imprimeLog(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        imprimeLog("Fim do Processamento do arquivo " + arquivo.getName());
    }

}
