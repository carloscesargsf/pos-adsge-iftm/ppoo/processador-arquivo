package br.edu.iftm.processador.enums;

import br.edu.iftm.processador.core.processador.ProcessadorArquivo;
import br.edu.iftm.processador.core.processador.arquivoClientes.ProcessadorArquivoClientes;
import br.edu.iftm.processador.core.processador.arquivoContas.ProcessadorArquivoContas;
import br.edu.iftm.processador.core.processador.arquivoPlasticos.ProcessadorArquivoPlasticos;
import br.edu.iftm.processador.core.processador.arquivoTransacoes.ProcessadorArquivoTransacoes;
import br.edu.iftm.processador.exception.ProcessadorDeArquivosException;

import java.util.Map;
import java.util.TreeMap;

public enum ArquivoProcessamento {

    ARQUIVO_CLIENTES(1, "CLI",
            "%s_clientes.txt", new ProcessadorArquivoClientes()),
    ARQUIVO_CONTAS(2, "CTA",
            "%s_contas.txt", new ProcessadorArquivoContas()),
    ARQUIVO_PLASTICOS(3, "PLA",
            "%s_plasticos.txt", new ProcessadorArquivoPlasticos()),
    ARQUIVO_TRANSACOES(4, "TRA",
            "%s_transacoes.txt", new ProcessadorArquivoTransacoes());

    private final Integer ordemExecucao;

    private final String tipoArquivo;

    private final String padraoNomeArquivo;

    private final ProcessadorArquivo processadorArquivo;

    private static Map<Integer, ArquivoProcessamento> arquivosProcessamento = new TreeMap<>();

    ArquivoProcessamento(Integer ordemExecucao, String tipoArquivo, String padraoNomeArquivo,
                         ProcessadorArquivo processadorArquivo) {
        this.ordemExecucao = ordemExecucao;
        this.tipoArquivo = tipoArquivo;
        this.padraoNomeArquivo = padraoNomeArquivo;
        this.processadorArquivo = processadorArquivo;
    }

    public Integer getOrdemExecucao() {
        return ordemExecucao;
    }

    public String getTipoArquivo() {
        return tipoArquivo;
    }

    public String getPadraoNomeArquivo() {
        return padraoNomeArquivo;
    }

    public ProcessadorArquivo getProcessadorArquivo() {
        return processadorArquivo;
    }

    static {
        for (ArquivoProcessamento arquivoProcessamento : ArquivoProcessamento.values()) {
            if (null != arquivosProcessamento.get(arquivoProcessamento.ordemExecucao)) {
                throw new ProcessadorDeArquivosException("Dois ou mais arquivos com mesma ordem de execução!");
            }

            arquivosProcessamento.put(arquivoProcessamento.ordemExecucao, arquivoProcessamento);
        }
    }

    public static Map<Integer, ArquivoProcessamento> getArquivosProcessamento() {
        return arquivosProcessamento;
    }

}
